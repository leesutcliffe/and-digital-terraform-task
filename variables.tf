variable "vpc_cidr" {
    default = "10.0.0.0/16"
}

variable "vpc_name" {
    default = "demo_vpc"
}

variable "web_app_name" {
    default = "nginx-web"
}

variable "aws_region" {
    description = "region for project"
    default = "eu-west-2"
}

variable "default_route" {
  default = "0.0.0.0/0"
}

variable "nginx_ami" {
    description = "NGINX Open Source Certified by Bitnami eu-west-2"
    default = "ami-0207e8980eb08715f"
}

variable "log_bucket" {
    default = "lb-log-bucket"
}

variable "net_eu_west_2a" {
  type = map
  default = {
    av_zone = "eu-west-2a"
    priv_net = "10.0.10.0/24"
    priv_name = "priv_subnet_web_2a"
    pub_net = "10.0.1.0/24"
    pub_name = "pub_subnet_web_2a"
  }
}

variable "net_eu_west_2b" {
  type = map
  default = {
    av_zone = "eu-west-2b"
    priv_net = "10.0.20.0/24"
    priv_name = "priv_subnet_web_2b"
    pub_net = "10.0.2.0/24"
    pub_name = "pub_subnet_web_2b"
  }
}

variable "net_eu_west_2c" {
  type = map
  default = {
    av_zone = "eu-west-2c"
    priv_net = "10.0.30.0/24"
    priv_name = "priv_subnet_web_2c"
    pub_net = "10.0.3.0/24"
    pub_name = "pub_subnet_web_2c"
  }
}

variable "instance_web1" {
  type = map
  default = {
    type = "t2.micro"
    name = "web1"
    av_zone = "eu-west-2a"
  }
}

variable "instance_web2" {
  type = map
  default = {
    type = "t2.micro"
    name = "web2"
    av_zone = "eu-west-2b"
  }
}

variable "instance_web3" {
  type = map
  default = {
    type = "t2.micro"
    name = "web3"
    av_zone = "eu-west-2c"
  }
}