
module "eu-west-2a_net" {
  source = "./modules/net"

  av_zone = var.net_eu_west_2a["av_zone"]
  priv_subnet = var.net_eu_west_2a["priv_net"]
  priv_subnet_name = var.net_eu_west_2a["priv_name"]
  pub_subnet = var.net_eu_west_2a["pub_net"]
  pub_subnet_name = var.net_eu_west_2a["pub_name"]
  vpc_id = aws_vpc.demo_vpc.id
}

module "eu-west-2b_net" {
  source = "./modules/net"

  av_zone = var.net_eu_west_2b["av_zone"]
  priv_subnet = var.net_eu_west_2b["priv_net"]
  priv_subnet_name = var.net_eu_west_2b["priv_name"]
  pub_subnet = var.net_eu_west_2b["pub_net"]
  pub_subnet_name = var.net_eu_west_2b["pub_name"]
  vpc_id = aws_vpc.demo_vpc.id
}

module "eu-west-2c_net" {
  source = "./modules/net"

  av_zone = var.net_eu_west_2c["av_zone"]
  priv_subnet = var.net_eu_west_2c["priv_net"]
  priv_subnet_name = var.net_eu_west_2c["priv_name"]
  pub_subnet = var.net_eu_west_2c["pub_net"]
  pub_subnet_name = var.net_eu_west_2c["pub_name"]
  vpc_id = aws_vpc.demo_vpc.id
}

module "eu-west-2a_web" {
  source = "./modules/web"

  ami = var.nginx_ami
  av_zone = var.instance_web1["av_zone"]
  instance_name = var.instance_web1["name"]
  instance_type = var.instance_web1["type"]
  pub_subnet_id = module.eu-west-2a_net.pub_subnet_id
  priv_subnet_id = module.eu-west-2a_net.priv_subnet_id
  vpc_cidr = var.vpc_cidr
  vpc_id = aws_vpc.demo_vpc.id
}

module "eu-west-2b_web" {
  source = "./modules/web"

  ami = var.nginx_ami
  av_zone = var.instance_web2["av_zone"]
  instance_name = var.instance_web2["name"]
  instance_type = var.instance_web2["type"]
  pub_subnet_id = module.eu-west-2b_net.pub_subnet_id
  priv_subnet_id = module.eu-west-2b_net.priv_subnet_id
  vpc_cidr = var.vpc_cidr
  vpc_id = aws_vpc.demo_vpc.id
}

module "eu-west-2c_web" {
  source = "./modules/web"

  ami = var.nginx_ami
  av_zone = var.instance_web3["av_zone"]
  instance_name = var.instance_web3["name"]
  instance_type = var.instance_web3["type"]
  pub_subnet_id = module.eu-west-2c_net.pub_subnet_id
  priv_subnet_id = module.eu-west-2c_net.priv_subnet_id
  vpc_cidr = var.vpc_cidr
  vpc_id = aws_vpc.demo_vpc.id
}

module "lb" {
  source = "./modules/lb"
  bucket = var.log_bucket

  instances = list(
    module.eu-west-2a_web.instance_id,
    module.eu-west-2b_web.instance_id,
    module.eu-west-2c_web.instance_id
  )

  pub_subnets = list(
    module.eu-west-2a_net.pub_subnet_id,
    module.eu-west-2b_net.pub_subnet_id,
    module.eu-west-2c_net.pub_subnet_id
  )

  vpc_id = aws_vpc.demo_vpc.id
  web_app = var.web_app_name
}