provider "aws" {
  shared_credentials_file = "~/.aws/credentials"
  profile = "default"
  region = var.aws_region
}

resource "aws_vpc" "demo_vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true
  tags = {
    Name = var.vpc_name
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.demo_vpc.id
  tags = {
    Name = "default gw"
  }
}

resource "aws_route" "internet_access" {
  route_table_id = aws_vpc.demo_vpc.main_route_table_id
  destination_cidr_block = var.default_route
  gateway_id = aws_internet_gateway.gw.id
}

resource "aws_s3_bucket" "log_bucket" {
  bucket = var.log_bucket
  acl  = "private"
}