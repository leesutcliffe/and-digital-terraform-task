## AND Digital Terraform Task

This Terraform plan deploys three web services in three different availability zones.

### AIM
The AIM used is NGINX Open Source Certified by Bitnami (ami-0207e8980eb08715f") - this requires agreeing to the EULA in the AWS console prior to deploying.

### Credentials
AWS credentials are referenced in the root ./main.tf file and refers to file ~/.aws/credentials. Change this file location and profile accordingly.

### LB DNS
AWS Load Balancer DNS address is exported and can be accessed using the following command
```terraform output```

alternatively, the following can be pasted into a command window to curl the LB to test for a response
```
fqdn=$(terraform output | grep lb_dns | awk '{print $3}' | sed 's/\"//g')
curl -I $fqdn
```
