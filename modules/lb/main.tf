resource "aws_lb" "lb" {
  name = "${var.web_app}-lb"
  subnets = var.pub_subnets
  security_groups = [aws_security_group.pub_lb_sg.id]

  access_logs {
    bucket = var.bucket
    prefix = "${var.web_app}_logs"
  }
}

data "aws_lb" "lb" {
  arn  = aws_lb.lb.arn
}

resource "aws_lb_listener" "lb_http_listener" {
  load_balancer_arn = aws_lb.lb.arn
  port = 80
  protocol = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.lb_target_group.arn
    type = "forward"
  }
}

resource "aws_lb_listener_rule" "host_based_routing" {
  listener_arn = aws_lb_listener.lb_http_listener.arn

  action {
    type = "forward"
    target_group_arn = aws_lb_target_group.lb_target_group.arn
  }

  condition {
    path_pattern {
      values = ["/"]
    }
  }
}

resource "aws_lb_target_group" "lb_target_group" {
  name = "${var.web_app}-http-group"
  port = 80
  protocol = "HTTP"
  vpc_id = var.vpc_id

  health_check {
    healthy_threshold = 3
    unhealthy_threshold = 10
    timeout = 5
    interval = 10
    path = "/"
    port = 80
  }
}

resource "aws_lb_target_group_attachment" "web" {
  count = length(var.instances)
  target_id = var.instances[count.index]
  target_group_arn = aws_lb_target_group.lb_target_group.arn
  port = 80
}

resource "aws_security_group" "pub_lb_sg" {
  name = "${var.web_app}-lb-sg"
  description = "Security group for Application Load Balancer"
  vpc_id = var.vpc_id

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.web_app}-lb-sg"
  }
}