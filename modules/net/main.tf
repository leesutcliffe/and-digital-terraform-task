resource "aws_subnet" "pub_subnet" {
  vpc_id = var.vpc_id
  cidr_block = var.pub_subnet
  availability_zone = var.av_zone
  map_public_ip_on_launch = true
  tags = {
      Name = var.pub_subnet_name
  }
}

resource "aws_subnet" "priv_subnet" {
  vpc_id = var.vpc_id
  cidr_block = var.priv_subnet
  availability_zone = var.av_zone
  tags = {
      Name = var.priv_subnet_name
  }
}