variable "ami" {}
variable "av_zone" {}
variable "instance_name" {}
variable "instance_type" {}
variable "priv_subnet_id" {}
variable "pub_subnet_id" {}
variable "vpc_cidr" {}
variable "vpc_id" {}