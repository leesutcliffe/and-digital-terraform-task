resource "aws_instance" "web" {
  ami = var.ami
  associate_public_ip_address = true
  availability_zone = var.av_zone
  instance_type = var.instance_type
  subnet_id = var.priv_subnet_id
  vpc_security_group_ids = [aws_security_group.web_sg.id]

  tags = {
    Name = var.instance_name
  }
}

resource "aws_security_group" "web_sg" {
  name = "${var.instance_name}-sg"
  description = "Security group for ${var.instance_name}"
  vpc_id = var.vpc_id

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  tags = {
    Name = "${var.instance_name}-sg"
  }
}